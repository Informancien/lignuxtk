#!/usr/bin/env bash

################################################################################
# File: jdetect
# Description: Heuristic Java detecting on Debian to check for Log4Shell
# Maintainer: Léo DALECKI
# License: MIT License
################################################################################

################################################################################
# Functions
################################################################################

# Get reverse dependencies of package $1
function rdepends() {
	
	local tab="$tab " pkgs=

	printf '%s%s\n' "$tab" "$1"

	# Checking if rdepends have already been listed
	if [[ "$rdepends" != "${rdepends/ $1 /}" ]]; then
		printf '%s (rdepends above)\n' "$tab"
		return
	fi

	# Mark so that rdepends won't be listed again
	rdepends="$rdepends $1 "

	# Reverse dependencies on installed packages
	while read; do
		if [[ "${REPLY%%:*}" == "  Depends" ]]; then
			local pkg="${REPLY##  Depends: }"
			pkg="${pkg%% (*)*}"

			# Depends might list the same package twice, fixes that
			if [[ $pkgs == "${pkgs/ $pkg /}" ]]; then
				pkgs="$pkgs $pkg "

				# Reverse dependencies on each dependent package 
				rdepends "$pkg"
			fi
		fi
	done < <(apt --installed rdepends $1 2>/dev/null)
}

################################################################################
# Main
################################################################################

export LANG=C

java_path="$(which java)"
java_bin="$(readlink -f $java_path)"
java_pkg="$(dpkg-query -S $java_bin | cut -d: -f1)"

rdepends=

# Summary
printf 'User: %s \nUID: %s \nUsers: %s\nWhich: %s\nBinary: %s\nPackage: %s\n' \
	"$USER" "$UID" "$(users)" "$java_path" "$java_bin" "$java_pkg"

# Installed packages depending on Java
printf 'Rdepends:\n'
rdepends "$java_pkg"

# Processes which commands contain "java"
printf 'Processes:\n'
ps -eo ' %u %g %p %a' | grep -i '[j]ava'

# Executables files containing "java" (and not javascript) in their name
printf 'Executables:\n'
find / -type f -iname '*java*' -executable -a \! -iname '*javascript*' \
	-exec file -i \{\} \; 2>/dev/null | while read; do
	printf ' %s\n' "$REPLY"
done
