# LiGNUxTK

GNU/Linux toolkit, a collection of utility scripts not warranting their own 
repositories.

## Repository structure

Each sub-directory of this repository contains distribution-specific scripts,
except for `all/`, of which scripts **should** run on any GNU/Linux system (no
exhaustive testing is done by the maintainer).

For disctribution-specific scripts to run properly, their directory name should
be same string as the value of the `ID` field in the `/etc/os-release` or
`/usr/lib/os-release` file (see **os-release(5)**).

## Scripts

Most scripts offer the `-h` or `--help` option, which will print out a summary
of its usage.

Here is a quick summary of the available scripts:
* `all/` Distribution agnostic scripts:
	* `zenity-luks` zenity and pkexec based GUI tool to change LUKS keys,
	* `fbx64-install` Setup and configure EFI fallback bootloader
	  `fbx64.efi`;
* `debian/` Debian scripts:
	* `jdetect` Heuristic detection of Java to assert potential
	  impact on security,
	* `deptree` Recursively list dependencies or reverse dependencies
	  of a package.

## License

This repository's scripts are developed by Léo DALECKI and provided under the
MIT License (also known as the Expat License).
